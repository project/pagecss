# PageCSS

## About PageCSS
PageCSS is an utility module for Drupal that allows you to define additional CSS styles for an URL. This module provides you an admin interface where you can add CSS styles that will be injected only for that URL. You can have multiple such rules.
This module can help you inject CSS styles in a page without requiring it to be written in a file. Also, you can add the entire page style for an URL using this module. Loading full page css using this module IS NOT the right approach, but can be helpful for debugging and super urgent big fixes (without a code deploy). Please give this module a try and use it for the purpose you see fit.

## Developed and Maintained by
Ramit Mitra <ramit@ramit.io>
