<?php

namespace Drupal\pagecss\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Url;

/**
 * Form handler for the Example add and edit forms.
 */
class ListForm extends ConfigFormBase
{
  
    /**  
     * {@inheritdoc}  
     */  
    protected function getEditableConfigNames()
    {  
        return [  
        'pagecss.adminsettings',  
        ];
    }  

    /**  
     * {@inheritdoc}  
     */  
    public function getFormId()
    {  
        return 'pagecss_overview_form';  
    } 
  
    /**  
     * {@inheritdoc}  
     */  
    public function buildForm(array $form, FormStateInterface $form_state)
    {  
        $config = $this->config('pagecss.adminsettings'); 
        // Get master list
        $masterList = $this->config('pagecss.adminsettings')  
            ->get('urlMasterList');

        $header = [
        'Route' => t('route'),
        'Style' => t('style'),
        'Media' => t('media type'),
        'Action' => t('actions'),
        '' => t(' ')
        ];
    
        // Initialize an empty array
        $output = [];
        // Next, loop through the $results array
        foreach ($masterList as $key => $item) {
            $output[$key] = [
            'Route' => $item,
            'Style' => $this->config('pagecss.adminsettings')->get($item),
            'Media' => $this->config('pagecss.adminsettings')->get($item . '-media'),
            'Action' => [
            new FormattableMarkup(
                '<a href=":link">@name</a>',
                [':link' => '/admin/config/pagecss/' . $key . '/edit', '@name' => 'Edit']
            ),
            new FormattableMarkup(
                '<a href=":link">@name</a>',
                [':link' => '/admin/config/pagecss/' . $key . '/delete', '@name' => 'Delete']
            )
            ]
            ];
        }
    
        $form['addnew'] = [
        '#title' => $this->t('Add new configuration'),
        '#type' => 'link',
        '#url' => \Drupal\Core\Url::fromRoute('pagecss.add_configuration'),
        '#weight' => 1
        ];
        $form['message'] = [
        '#type' => 'item',
        '#markup' => $this->t('Presently active PageCSS rules'),
        '#weight' => 2,
        ];
        $form['table'] = [
        '#type' => 'tableselect',
        '#weight' => 5,
        '#header' => $header,
        '#options' => $output,
        '#empty' => t('No configurations found'),
        ];

        return parent::buildForm($form, $form_state);  
    } 
  
    /**  
     * {@inheritdoc}  
     */  
    public function submitForm(array &$form, FormStateInterface $form_state)
    {  
        parent::submitForm($form, $form_state);
    }
}
