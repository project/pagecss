<?php

namespace Drupal\pagecss\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;

/**
 * Form handler for the Example add and edit forms.
 */
class AddRecordForm extends ConfigFormBase
{
  
    /**  
     * {@inheritdoc}  
     */  
    protected function getEditableConfigNames()
    {  
        return [  
        'pagecss.adminsettings',  
        ];
    }  

    /**  
     * {@inheritdoc}  
     */  
    public function getFormId()
    {  
        return 'pagecss_add_record_form';  
    }
  
    /**  
     * {@inheritdoc}  
     */  
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $form['url'] = [  
        '#type' => 'textfield',  
        '#title' => $this->t('URL'),
        '#required' => true,        
        '#description' => $this->t('Aliased URL for which the below styles will be applied')
        ];
        $form['css_style'] = [  
        '#type' => 'textarea',  
        '#title' => $this->t('Style'),
        '#required' => true,
        '#description' => $this->t('CSS styles to be applied'),
        ];
        $form['media'] = [
        '#type' => 'select',
        '#title' => $this->t('Media'),
        '#required' => true,
        '#options' => [
        'all' => $this->t('All'),
        'print' => $this->t('Print'),
        'screen' => $this->t('Screen'),
        'projection' => $this->t('Projection'),
        'handheld' => $this->t('Handheld'),
        'braille' => $this->t('Braille'),
        ],
        '#default_value' => 'all',
        '#description' => $this->t('Allows CSS styles to be optimized for a variety of devices and media formats'),
        ];

        return parent::buildForm($form, $form_state);  
    }
  
    /**
     * Validate the checkbox of the form
     * 
     * @param array                                $form
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     */
    public function validateForm(array &$form, FormStateInterface $form_state)
    {
        parent::validateForm($form, $form_state);
        $url = $form_state->getValue('url');
        if (preg_match('/[\/*]/', $url) != 1) {
            // Set an error for the form element with a key of "accept".
            $form_state->setErrorByName('url', $this->t("Aliased URL should start with a '/', like /about"));
        }
    }
  
    /**  
     * {@inheritdoc}  
     */  
    public function submitForm(array &$form, FormStateInterface $form_state)
    {  
        parent::submitForm($form, $form_state);
    
        /* get master url list */
        $masterList = $this->config('pagecss.adminsettings')  
            ->get('urlMasterList');
        $thisUrl = $form_state->getValue('url');
        trim($thisUrl, " ");
        
        if(empty($masterList)) {
            $masterList = [];
        }
        $masterList[] = $thisUrl;
    
        $this->config('pagecss.adminsettings')  
            ->set('urlMasterList', $masterList)  
            ->set($thisUrl, $form_state->getValue('css_style'))
            ->set($thisUrl . '-media', $form_state->getValue('media'))
            ->save();
      
        $url = \Drupal\Core\Url::fromRoute('pagecss.overview', []);
        return $form_state->setRedirectUrl($url);
    }
}
