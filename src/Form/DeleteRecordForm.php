<?php

namespace Drupal\pagecss\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Url;

/**
 * Form handler for the Example add and edit forms.
 */
class DeleteRecordForm extends ConfigFormBase
{
  
    /**  
     * {@inheritdoc}  
     */
    protected function getEditableConfigNames()
    {  
        return [  
        'pagecss.adminsettings',  
        ];
    }  

    /**  
     * {@inheritdoc}  
     */  
    public function getFormId()
    {  
        return 'pagecss_delete_record_form';  
    } 
  
    /**  
     * {@inheritdoc}  
     */  
    public function buildForm(array $form, FormStateInterface $form_state, $id = null)
    {
    
        $form['recordID'] = array(
        '#type' => 'hidden',
        '#value' => $id,
        );
        $form['description'] = [
        '#type' => 'item',
        '#markup' => $this->t('Are you sure you want to delete this configuration ?'),
        ];
        $form['accept'] = array(
        '#type' => 'checkbox',
        '#title' => $this
        ->t('Confirm delete'),
        '#description' => $this->t('This action cannot be undone'),
        );
        $form['actions'] = [
        '#type' => 'actions',
        ];
        $form['actions']['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Delete'),
        ];

        return parent::buildForm($form, $form_state);  
    }
  
    /**
     * Validate the checkbox of the form
     * 
     * @param array                                $form
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     */
    public function validateForm(array &$form, FormStateInterface $form_state)
    {
        parent::validateForm($form, $form_state);
    
        $accept = $form_state->getValue('accept');

        if (!$accept) {
            // Set an error for the form element with a key of "accept".
            $form_state->setErrorByName('accept', $this->t('Please tick the checkbox to confirm'));
        }
    }
  
    /**  
     * {@inheritdoc}  
     */  
    public function submitForm(array &$form, FormStateInterface $form_state)
    {  
        parent::submitForm($form, $form_state);
    
        /* get master url list */
        $masterList = $this->config('pagecss.adminsettings')  
            ->get('urlMasterList');
        $recordID = $form_state->getValue('recordID');
        
        /* delete configuration */
        $this->config('pagecss.adminsettings')
            ->clear($masterList[$recordID] . '-media')
            ->clear($masterList[$recordID])
            ->save();
        unset($masterList[$recordID]);
        /* save back */
        $this->config('pagecss.adminsettings')  
            ->set('urlMasterList', $masterList)  
            ->save();
          
        /* redirect to listing */
        $url = \Drupal\Core\Url::fromRoute('pagecss.overview', []);
        return $form_state->setRedirectUrl($url);
    }
}
